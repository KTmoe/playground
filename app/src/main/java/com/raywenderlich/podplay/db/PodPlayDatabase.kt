package com.raywenderlich.podplay.db

import android.arch.persistence.room.*
import android.content.Context
import com.raywenderlich.podplay.db.converters.Converters
import com.raywenderlich.podplay.model.Episode
import com.raywenderlich.podplay.model.Podcast

@Database(entities = [Podcast::class, Episode::class], version = 1)
@TypeConverters(Converters::class)
abstract class PodPlayDatabase: RoomDatabase() {

    abstract fun podcastDao(): PodcastDao

    companion object{
        private var instance: PodPlayDatabase? = null
        fun getInstance(context: Context) : PodPlayDatabase{
            if(instance == null){
                instance = Room.databaseBuilder(
                        context.applicationContext,
                        PodPlayDatabase::class.java,
                        "PodPlayer")
                        .allowMainThreadQueries()
                        .build()
            }
            return instance as PodPlayDatabase
        }
    }
}