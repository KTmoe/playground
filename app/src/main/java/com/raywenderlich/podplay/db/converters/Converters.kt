package com.raywenderlich.podplay.db.converters

import android.arch.persistence.room.TypeConverter
import java.util.*

class Converters {
    @TypeConverter
    fun fromTimeStamp(value: Long?) : Date? {
        return if(value == null) null else Date(value)
    }
    @TypeConverter
    fun toTimeStamp(date: Date?) : Long?{
        return date?.time
    }
}