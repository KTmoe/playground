package com.raywenderlich.podplay.ui


import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.ComponentName
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.SeekBar
import com.bumptech.glide.Glide

import com.kaytharimyatmoe.podplay.R
import com.raywenderlich.podplay.service.PodplayMediaService
import com.raywenderlich.podplay.util.DateUtils
import com.raywenderlich.podplay.util.HtmlUtils
import com.raywenderlich.podplay.viewmodel.PodcastViewModel
import kotlinx.android.synthetic.main.fragment_episode_player.*

/**
 * A simple [Fragment] subclass.
 */
class EpisodePlayerFragment : Fragment() {

    private lateinit var podcastViewModel: PodcastViewModel
    private lateinit var mediaBrowser: MediaBrowserCompat
    private var mediaControllerCallback: MediaControllerCallback? = null
    private var playerSpeed: Float = 1.0f
    private var episodeDuration: Long = 0
    private var draggingScrubber: Boolean = false
    private var progressAnimator: ValueAnimator? = null

    private fun animateScrubber(progress: Int, speed: Float){
        val timeRemaining = ((episodeDuration - progress) / speed).toInt()
        if(timeRemaining < 0){ return }
        progressAnimator = ValueAnimator.ofInt(
                progress, episodeDuration.toInt()
        )
        progressAnimator?.let { animator ->
            animator.duration = timeRemaining.toLong()
            animator.interpolator = LinearInterpolator()
            animator.addUpdateListener {
                if(draggingScrubber){
                    animator.cancel()
                } else {
                    seekBar.progress = animator.animatedValue as Int
                }
            }
            animator.start()
        }
    }

    private fun setupViewModel(){
        val fragmentActivity = activity as FragmentActivity
        podcastViewModel = ViewModelProviders.of(fragmentActivity)
                .get(PodcastViewModel::class.java)
    }

    @SuppressLint("SetTextI18n")
    private fun updateControls(){
        episodeTitleTextView.text =
                podcastViewModel.activeEpisodeViewData?.title
        val htmlDesc =
                podcastViewModel.activeEpisodeViewData?.description ?: ""
        val descSpan = HtmlUtils.htmlToSpannable(htmlDesc)
        episodeDescTextView.text = descSpan
        episodeDescTextView.movementMethod = ScrollingMovementMethod()
        val fragmentActivity = activity as FragmentActivity
        Glide.with(fragmentActivity)
                .load(podcastViewModel.activePodcastViewData?.imageUrl)
                .into(episodeImageView)
        speedButton.text = "${playerSpeed}X"
    }

    private fun startPlaying(
            episodeViewData: PodcastViewModel.EpisodeViewData
    ){
        val fragmentActivity = activity as FragmentActivity
        val controller = MediaControllerCompat.getMediaController(fragmentActivity)
        val viewData = podcastViewModel.activePodcastViewData ?: return
        val bundle = Bundle()
        bundle.putString(MediaMetadataCompat.METADATA_KEY_TITLE,
                episodeViewData.title)
        bundle.putString(MediaMetadataCompat.METADATA_KEY_ARTIST,
                viewData.feedTitle)
        bundle.putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI,
                viewData.imageUrl)
        controller.transportControls.playFromUri(
                Uri.parse(episodeViewData.mediaUrl), bundle
        )
    }

    @SuppressLint("SetTextI18n")
    private fun changeSpeed(){
        playerSpeed += 0.25f
        if(playerSpeed > 2.0f){
            playerSpeed = 0.75f
        }
        val bundle = Bundle()
        bundle.putFloat(CMD_EXTRA_SPEED, playerSpeed)
        val fragmentActivity = activity as FragmentActivity
        val controller =
                MediaControllerCompat.getMediaController(fragmentActivity)
        controller.sendCommand(CMD_CHANGESPEED, bundle, null)
        speedButton.text = "${playerSpeed}X"
    }

    companion object{
        fun newInstance(): EpisodePlayerFragment{
            return EpisodePlayerFragment()
        }
        const val CMD_EXTRA_SPEED = "speed"
        const val CMD_CHANGESPEED = "change_speed"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        setupViewModel()
        initMediaBrowser()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_episode_player, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupControls()
        updateControls()
    }

    override fun onStart() {
        super.onStart()
        Log.d("MEDIAPLAYER","You are at OnStart")
        Log.d("MEDIAPLAYER","${mediaBrowser.isConnected}")
        if(mediaBrowser.isConnected){
            val fragmentActivity = activity as FragmentActivity
            if(MediaControllerCompat.getMediaController(fragmentActivity) == null){
                registerMediaController(mediaBrowser.sessionToken)
            }
            updateControlsFromController()
        }else{
            Log.d("MEDIAPLAYER","MediaBrowser Connect")
            mediaBrowser.connect()
        }
    }

    override fun onStop() {
        super.onStop()
        progressAnimator?.cancel()
        val fragmentActivity = activity as FragmentActivity
        if(MediaControllerCompat.getMediaController(fragmentActivity) != null){
            mediaControllerCallback?.let {
                MediaControllerCompat.getMediaController(fragmentActivity)
                        .unregisterCallback(it)
            }
        }
    }

    private fun initMediaBrowser(){
        val fragmentActivity = activity as FragmentActivity
        mediaBrowser = MediaBrowserCompat(fragmentActivity,
                ComponentName(fragmentActivity, PodplayMediaService::class.java),
                MediaBrowserCallBacks(), null)
    }

    private fun registerMediaController(token: MediaSessionCompat.Token){
        Log.d("MEDIAPLAYER","registerMediaController")
        val fragmentActivity = activity as FragmentActivity
        val mediaController = MediaControllerCompat(fragmentActivity, token)
        MediaControllerCompat.setMediaController(fragmentActivity,
                mediaController)
        mediaControllerCallback = MediaControllerCallback()
        mediaController.registerCallback(mediaControllerCallback!!)
    }

    private fun togglePlayPause(){
        val fragmentActivity = activity as FragmentActivity
        val controller = MediaControllerCompat.getMediaController(fragmentActivity)
        if(controller.playbackState != null){
            if(controller.playbackState.state ==
                    PlaybackStateCompat.STATE_PLAYING){
                controller.transportControls.pause()
            } else{
                podcastViewModel.activeEpisodeViewData?.let {
                    startPlaying(it)
                }
            }
        }else {
            podcastViewModel.activeEpisodeViewData?.let { startPlaying(it) }
        }
    }

    private fun setupControls(){
        playToggleButton.setOnClickListener { togglePlayPause() }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            speedButton.setOnClickListener {
                changeSpeed()
            }
        } else {
            speedButton.visibility = View.INVISIBLE
        }
        forwardButton.setOnClickListener { seekBy(30) }
        replayButton.setOnClickListener { seekBy(-10) }
        seekBar.setOnSeekBarChangeListener(
                object : SeekBar.OnSeekBarChangeListener {
                    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                        currentTimeTextView.text = android.text.format.DateUtils.formatElapsedTime(
                                (progress / 1000).toLong()
                        )
                    }

                    override fun onStartTrackingTouch(seekBar: SeekBar?) {
                        draggingScrubber = true
                    }

                    override fun onStopTrackingTouch(seekBar: SeekBar?) {
                        draggingScrubber = false
                        val fragmentActivity = activity as FragmentActivity
                        val controller = MediaControllerCompat.getMediaController(fragmentActivity)
                        if(controller.playbackState != null){
                            controller.transportControls.seekTo(seekBar!!.progress.toLong())
                        } else {
                            seekBar!!.progress = 0
                        }
                    }

                }
        )
    }

    private fun seekBy(seconds: Int) {
        val fragmentActivity = activity as FragmentActivity
        val controller =
                MediaControllerCompat.getMediaController(fragmentActivity)
        val newPosition = controller.playbackState.position + seconds * 1000
        controller.transportControls.seekTo(newPosition)
    }

    @SuppressLint("SetTextI18n")
    private fun handleStateChanged(state: Int, position: Long, speed: Float){
        val isPlaying = state == PlaybackStateCompat.STATE_PLAYING
        playToggleButton.isActivated = isPlaying
        val progress = position.toInt()
        seekBar.progress = progress
        speedButton.text = "${playerSpeed}X"
        if(isPlaying){
            animateScrubber(progress, speed)
        }
        progressAnimator?.let {
            it.cancel()
            progressAnimator = null
        }
    }

    private fun updateControlsFromMetadata(metadata: MediaMetadataCompat){
        episodeDuration =
                metadata.getLong(MediaMetadataCompat.METADATA_KEY_DURATION)
        endTimeTextView.text = android.text.format.DateUtils.formatElapsedTime(
                episodeDuration / 1000
        )
        seekBar.max = episodeDuration.toInt()
    }

    private fun updateControlsFromController(){
        val fragmentActivity = activity as FragmentActivity
        val controller =
                MediaControllerCompat.getMediaController(fragmentActivity)
        if(controller != null){
            val metadata = controller.metadata
            if(metadata != null){
                handleStateChanged(controller.playbackState.state,
                        controller.playbackState.position, playerSpeed)
                updateControlsFromMetadata(controller.metadata)
            }
        }
    }

    inner class MediaControllerCallback: MediaControllerCompat.Callback(){
        override fun onMetadataChanged(metadata: MediaMetadataCompat?) {
            super.onMetadataChanged(metadata)
            Log.d("MEDIAPLAYER","metadata changed to ${metadata?.getString(
                    MediaMetadataCompat.METADATA_KEY_MEDIA_URI
            )}")
            metadata?.let { updateControlsFromMetadata(it) }
        }

        override fun onPlaybackStateChanged(state: PlaybackStateCompat?) {
            super.onPlaybackStateChanged(state)
            Log.d("MEDIAPLAYER","state changed to $state")
            val state = state ?: return
            handleStateChanged(state.state, state.position, state.playbackSpeed)
        }
    }

    inner class MediaBrowserCallBacks:
            MediaBrowserCompat.ConnectionCallback(){
        override fun onConnected() {
            super.onConnected()
            registerMediaController(mediaBrowser.sessionToken)
            Log.d("MEDIAPLAYER","onConnected")
            updateControlsFromController()
        }

        override fun onConnectionSuspended() {
            super.onConnectionSuspended()
            Log.d("MEDIAPLAYER","onConnectionSuspended")
        }

        override fun onConnectionFailed() {
            super.onConnectionFailed()
            Log.d("MEDIAPLAYER","onConnectionFailed")
        }
    }
}
